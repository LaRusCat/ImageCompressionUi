TEMPLATE = subdirs

SUBDIRS += \
    Lib \
    Ui \
    LibTest

Ui.depends = Lib
LibTest.depends = Lib

CONFIG += ordered

