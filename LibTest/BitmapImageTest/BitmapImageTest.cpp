#include "../../Lib/BitmapImage.h"
#include <QtTest>
#include <filesystem>

// add necessary includes here

class BitmapImageTest : public QObject {
    Q_OBJECT

public:
    BitmapImageTest();
    ~BitmapImageTest();

private slots:
    void loadSave();
};

BitmapImageTest::BitmapImageTest()
{
}

BitmapImageTest::~BitmapImageTest()
{
}

void BitmapImageTest::loadSave()
{
    BitmapImage image;
    image.loadFromFile("40x40.bmp");
    image.saveToFile("40x40Saved.bmp");
    BitmapImage savedImage;
    savedImage.loadFromFile("40x40Saved.bmp");
    QCOMPARE(image, savedImage);
}

QTEST_APPLESS_MAIN(BitmapImageTest)

#include "BitmapImageTest.moc"
