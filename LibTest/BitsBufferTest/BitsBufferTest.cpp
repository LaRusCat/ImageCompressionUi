#include "../../Lib/BitsBuffer.h"
#include <QTest>

class BitsBufferTest : public QObject {
    Q_OBJECT
private slots:
    void endOfBits();
    void readWriteBool();
    void readWriteChars();
    void readWriteMixed();
};

void BitsBufferTest::endOfBits()
{
    BitsBuffer bitsBuffer;
    QCOMPARE(bitsBuffer.endOfBits(), true);
    bitsBuffer << true;
    QCOMPARE(bitsBuffer.endOfBits(), false);
}

void BitsBufferTest::readWriteBool()
{
    BitsBuffer bitsBuffer;
    std::vector<bool> bools { true, true, false, false, true };
    for (const auto b : bools) {
        bitsBuffer << (bool)b; // cast form std::std:bit_reference
    }

    bitsBuffer.seekg(0);
    bool boolBuffer;
    for (size_t i = 0; i < bools.size(); ++i) {
        bitsBuffer >> boolBuffer;
        QCOMPARE(boolBuffer, bools[i]);
    }
}

void BitsBufferTest::readWriteChars()
{
    BitsBuffer bitsBuffer;
    std::vector<unsigned char> chars { 0xff, 0xff, 0xf7, 0xef, 0x01 };
    for (const auto c : chars) {
        bitsBuffer << c;
    }

    bitsBuffer.seekg(0);
    unsigned char charBuffer;
    for (size_t i = 0; i < chars.size(); ++i) {
        bitsBuffer >> charBuffer;
        QCOMPARE(charBuffer, chars[i]);
    }
}

void BitsBufferTest::readWriteMixed()
{
    BitsBuffer bitsBuffer;
    bool b1 = true;
    bool b2 = false;
    unsigned char c1 = 0xfc;
    unsigned char c2 = 0x02;

    bitsBuffer << b1 << b2 << c1 << c2;

    bool boolBuffer;
    unsigned char charBuffer;
    bitsBuffer >> boolBuffer;
    QCOMPARE(boolBuffer, b1);

    bitsBuffer >> boolBuffer;
    QCOMPARE(boolBuffer, b2);

    bitsBuffer >> charBuffer;
    QCOMPARE(charBuffer, c1);

    bitsBuffer >> charBuffer;
    QCOMPARE(charBuffer, c2);
}

QTEST_APPLESS_MAIN(BitsBufferTest)

#include "BitsBufferTest.moc"
