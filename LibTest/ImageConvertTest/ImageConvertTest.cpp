#include "../../Lib/ImageConvert.h"
#include <QTest>

class ImageConvertTest : public QObject {
    Q_OBJECT
private slots:
    void convert_fromBitmapToBarch();
    void convert_fromBarchToBitmap();
};

void ImageConvertTest::convert_fromBitmapToBarch()
{
    const size_t width = 8;
    const size_t height = 4;
    // clang-format off
    const BitmapImage bitmap(width, height, std::vector<unsigned char> {
                                 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                                 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
                                 0x01, 0x02, 0x03, 0xff, 0x00, 0x00, 0x00, 0x00,
                                 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                             });
    // clang-format on
    const auto barch = image::convertion::convert<BitmapImage, BarchImage>(bitmap);
    QCOMPARE(barch.width(), width);
    QCOMPARE(barch.height(), height);
    QCOMPARE(barch.emptinesBits().bytes(), std::vector<unsigned char> { 0b00001001 });
    BitsBuffer expectedDataBits;
    expectedDataBits << true << false; // 4 black pixels
    expectedDataBits << false; // 4 white pixels
    expectedDataBits << true << true; // 4 mixed pixels
    expectedDataBits << (unsigned char)0x01 << (unsigned char)0x02 << (unsigned char)0x03 << (unsigned char)0xff;
    expectedDataBits << true << false; // 4 black pixels
    QCOMPARE(barch.dataBits().bytes(), expectedDataBits.bytes());
}

void ImageConvertTest::convert_fromBarchToBitmap()
{
    const size_t width = 8;
    const size_t height = 4;
    BitsBuffer emptinesBits;
    emptinesBits << true << false << false << true;
    BitsBuffer dataBits;
    dataBits << true << false; // 4 black pixels
    dataBits << false; // 4 white pixels
    dataBits << true << true; // 4 mixed pixels
    dataBits << (unsigned char)0xfc << (unsigned char)0x02 << (unsigned char)0x03 << (unsigned char)0xff;
    dataBits << true << false; // 4 black pixels
    const BarchImage barch(width, height, emptinesBits, dataBits);

    const auto bitmap = image::convertion::convert<BarchImage, BitmapImage>(barch);
    QCOMPARE(bitmap.width(), width);
    QCOMPARE(bitmap.height(), height);

    // clang-format off
    std::vector<unsigned char> expectedData {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
        0xfc, 0x02, 0x03, 0xff, 0x00, 0x00, 0x00, 0x00,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    };
    // clang-format on

    QCOMPARE(bitmap.data(), expectedData);
}

QTEST_APPLESS_MAIN(ImageConvertTest)

#include "ImageConvertTest.moc"
