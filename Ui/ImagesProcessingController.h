#pragma once

#include <QObject>
#include <QSet>

class ImagesProcessingController : public QObject {
    Q_OBJECT
    Q_PROPERTY(QSet<QString> supportedExtensions READ supportedExtensions CONSTANT)
public:
    explicit ImagesProcessingController(QObject* parent = nullptr);

    Q_INVOKABLE void processFileAsync(const QString& filePath);

signals:
    void processingStarted(QString processedFilePath);
    void processingFinished(QString processedFilePath);
    void processingFailed(QString processedFilePath, QString errorText = "");

private:
    const QSet<QString> &supportedExtensions() const;
    void processFile(const QString& processedFilePath);
    void onProcessingStarted(const QString& processedFilePath);
    void onProcessingFinishedFailed(const QString& processedFilePath);

    QSet<QString> mProcessingFiles;
};
