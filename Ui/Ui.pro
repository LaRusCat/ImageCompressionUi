TEMPLATE = app
QT += quick

SOURCES += \
        FilesModel.cpp \
        ImagesProcessingController.cpp \
        main.cpp

RESOURCES += qml.qrc

HEADERS += \
    FilesModel.h \
    ImagesProcessingController.h

LIBS += -L../Lib -lLibImage
