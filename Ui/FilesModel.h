#pragma once

#include <QAbstractListModel>
#include <QDir>

class FilesModel : public QAbstractListModel {
    Q_OBJECT
public:
    enum FileType{
        Bmp,
        Batch,
        Other
    };
    Q_ENUMS(EnStyle)
    enum FileRoles {
        NameRole = Qt::UserRole + 1,
        PathRole,
        SizeRole,
        ExtensionRole
    };
    FilesModel(const QDir& dir, const QStringList& nameFilters, QObject* parent = nullptr);
    int rowCount(const QModelIndex& = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QFileInfoList mFileInfoList;
};
