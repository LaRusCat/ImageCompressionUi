#include "FilesModel.h"

FilesModel::FilesModel(const QDir& dir, const QStringList& nameFilters, QObject* parent)
    : QAbstractListModel(parent)
{
    mFileInfoList = dir.entryInfoList(nameFilters, QDir::Files, QDir::Name);
}


int FilesModel::rowCount(const QModelIndex& /*parent*/) const
{
    return mFileInfoList.size();
}

QVariant FilesModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= mFileInfoList.size())
      return QVariant();

    const auto fileInfo = mFileInfoList.at(index.row());
    switch (role) {
    case NameRole:
        return fileInfo.fileName();
    case PathRole:
        return fileInfo.absoluteFilePath();
    case SizeRole: {
        return fileInfo.size();
    }
    case ExtensionRole: {
        return fileInfo.completeSuffix();
    }
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> FilesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "fileName";
    roles[PathRole] = "filePath";
    roles[SizeRole] = "fileSize";
    roles[ExtensionRole] = "fileExtension";
    return roles;
}
