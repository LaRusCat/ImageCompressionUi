#include "ImagesProcessingController.h"
#include "../Lib/BarchImage.h"
#include "../Lib/BitmapImage.h"
#include "../Lib/ImageConvert.h"
#include <QDir>
#include <QThread>
#include <QtConcurrent/QtConcurrent>

#include <QFileInfo>

namespace {
const QString BMP_EXTENSION = QStringLiteral("bmp");
const QString BARCH_EXTENSION = QStringLiteral("barch");
const QMap<QString, QString> extensionToProcessedSuffixMap = {{BMP_EXTENSION, "-packed.barch"},{BARCH_EXTENSION, "-unpacked.bmp"}};
}

ImagesProcessingController::ImagesProcessingController(QObject* parent)
    : QObject(parent)
{
    connect(this, &ImagesProcessingController::processingStarted, this, &ImagesProcessingController::onProcessingStarted);
    connect(this, &ImagesProcessingController::processingFinished, this, &ImagesProcessingController::onProcessingFinishedFailed);
    connect(this, &ImagesProcessingController::processingFailed, this, &ImagesProcessingController::onProcessingFinishedFailed);
}

void ImagesProcessingController::processFileAsync(const QString& filePath)
{
    if (mProcessingFiles.contains(filePath)) {
        qWarning() << "processing already in progress for file:" << filePath;
        return;
    }
    static_cast<void>(QtConcurrent::run(&ImagesProcessingController::processFile, this, filePath));
}

const QSet<QString>& ImagesProcessingController::supportedExtensions() const
{
    static const QSet<QString> extensions = { BMP_EXTENSION, BARCH_EXTENSION };
    return extensions;
}

void ImagesProcessingController::processFile(const QString& processedFilePath)
{
    const QFileInfo fileInfo(processedFilePath);
    const auto completeSuffix = fileInfo.completeSuffix();
    if (!supportedExtensions().contains(completeSuffix)) {
        qDebug() << "not supported image extension:" << processedFilePath;
        return;
    }
    QFileInfo outputFileInfo = QFileInfo(fileInfo.dir(), fileInfo.baseName() + extensionToProcessedSuffixMap[completeSuffix]);

    emit processingStarted(processedFilePath);
    try {
        if (completeSuffix == BARCH_EXTENSION) {
            image::convertion::convert<BarchImage, BitmapImage>(fileInfo.filesystemAbsoluteFilePath(), outputFileInfo.filesystemAbsoluteFilePath());
        } else if (completeSuffix == BMP_EXTENSION) {
            image::convertion::convert<BitmapImage, BarchImage>(fileInfo.filesystemAbsoluteFilePath(), outputFileInfo.filesystemAbsoluteFilePath());
        }
    } catch (const std::runtime_error& error) {
        emit processingFailed(processedFilePath, QString(error.what()));
        return;
    }
    emit processingFinished(processedFilePath);
}

void ImagesProcessingController::onProcessingStarted(const QString& processedFilePath)
{
    mProcessingFiles.insert(processedFilePath);
}

void ImagesProcessingController::onProcessingFinishedFailed(const QString& processedFilePath)
{
    mProcessingFiles.remove(processedFilePath);
}
