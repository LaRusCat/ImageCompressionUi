import QtQuick
import QtQuick.Window
import QtQuick.Layouts

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Image compression")

    readonly property var extensionsToProcess: ["bmp", "barch"]
    readonly property Component dialog: Qt.createComponent("Dialog.qml")

    function showErrorDialog(fileName, error) {
        const primaryTextString = qsTr("File processing failed")
        const window = dialog.createObject(root, {
                                               "title": primaryTextString,
                                               "primaryText": error,
                                               "secondaryText": fileName
                                           })
        window.show()
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.fillWidth: true
            text: qsTr("Saved BMP files will be corrupted due to issues with file or BMP header saving")
            Rectangle {
                anchors.fill: parent
                z: -1
                color: "red"
            }
        }

        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: imagesModel
            spacing: 5

            delegate: Rectangle {
                width: parent.width
                height: layout.height
                color: "navajowhite"
                RowLayout {
                    id: layout
                    width: parent.width
                    spacing: 10
                    Text {
                        id: nameText
                        text: fileName
                    }
                    Text {
                        id: sizeText
                        text: qsTr("%1 bytes").arg(fileSize)
                    }
                    Text {
                        id: statusText
                        Layout.fillWidth: true
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (imagesProcessingController.supportedExtensions.includes(
                                    fileExtension)) {
                            imagesProcessingController.processFileAsync(
                                        filePath)
                        }
                    }
                }
                Connections {
                    target: imagesProcessingController
                    function onProcessingStarted(processedFilePath) {
                        if (processedFilePath === filePath) {
                            statusText.text = "processing"
                        }
                    }
                    function onProcessingFinished(processedFilePath) {
                        if (processedFilePath === filePath) {
                            statusText.text = "finished"
                        }
                    }
                    function onProcessingFailed(processedFilePath, errorText) {
                        if (processedFilePath === filePath) {
                            statusText.text = "failed"
                            root.showErrorDialog(fileName, errorText)
                        }
                    }
                }
            }
        }
    }
}
