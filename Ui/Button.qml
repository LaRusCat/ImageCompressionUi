import QtQuick

Text {
    id: root
    signal clicked
    padding: 20
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    Rectangle {
        id: background
        z: -1
        anchors.fill: parent
        color: mouseArea.pressed ? "darkturquoise" : "gold"
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
