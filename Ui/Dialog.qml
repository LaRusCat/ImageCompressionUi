import QtQuick
import QtQuick.Layouts

Window {
    id: root
    property alias primaryText: primaryLabel.text
    property alias secondaryText: secondaryLabel.text
    width: 400
    height: 200
    ColumnLayout {
        anchors.fill: parent
        Text {
            id: primaryLabel
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }
        Text {
            id: secondaryLabel
            Layout.fillHeight: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Button {
            text: qsTr("Ok")
            Layout.fillWidth: true
            onClicked: close()
        }
    }
}
