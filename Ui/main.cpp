#include "FilesModel.h"
#include "ImagesProcessingController.h"
#include <QDebug>
#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QDir dir = app.arguments().size() > 1 ? app.arguments().at(1) : QDir::currentPath();
    FilesModel imagesModel(dir, { "*.bmp", "*.png", "*.barch" });
    engine.rootContext()->setContextProperty("imagesModel", &imagesModel);

    ImagesProcessingController imagesProcessingController;
    engine.rootContext()->setContextProperty("imagesProcessingController", &imagesProcessingController);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject* obj, const QUrl& objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
