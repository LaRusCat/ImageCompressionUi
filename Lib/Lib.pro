CONFIG -= qt

TARGET = LibImage
TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++20

SOURCES += \
    BarchImage.cpp \
    BaseImage.cpp \
    BitmapImage.cpp

HEADERS += \
    BarchImage.h \
    BaseImage.h \
    BitmapImage.h \
    BitsBuffer.h \
    ImageConvert.h
