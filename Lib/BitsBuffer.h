#pragma once
#include <iostream>
#include <vector>

namespace {
const unsigned char BITS_IN_BYTE = 8;
const unsigned char LAST_BIT_IN_BYTE_POS = BITS_IN_BYTE - 1;

void incrementBitInBytePos(unsigned char& bitInBytePos)
{
    bitInBytePos = (bitInBytePos + 1) % BITS_IN_BYTE;
}
}

class BitsBuffer {
public:
    BitsBuffer()
        : mBytes()
        , mEndBitInBytePos(LAST_BIT_IN_BYTE_POS)
        , mCurrentReadBytePos(0)
        , mCurrentReadBitInBytePos(0)
    {
    }
    BitsBuffer(const std::vector<unsigned char>& bytes)
        : mBytes(bytes)
        , mEndBitInBytePos(LAST_BIT_IN_BYTE_POS)
        , mCurrentReadBytePos(0)
        , mCurrentReadBitInBytePos(0)
    {
    }

    BitsBuffer(std::vector<unsigned char>&& bytes)
        : mBytes(move(bytes))
        , mEndBitInBytePos(LAST_BIT_IN_BYTE_POS)
        , mCurrentReadBytePos(0)
        , mCurrentReadBitInBytePos(0)
    {
    }

    const std::vector<unsigned char>& bytes() const
    {
        return mBytes;
    }

    void seekg(size_t position)
    {
        if (position == 0) {
            mCurrentReadBytePos = 0;
            mCurrentReadBitInBytePos = 0;
        }
        mCurrentReadBytePos = position / BITS_IN_BYTE;
        mCurrentReadBitInBytePos = position % BITS_IN_BYTE;
    }

    bool endOfBits() const
    {
        if (mCurrentReadBytePos + 1 > mBytes.size())
            return true;
        if (((mCurrentReadBytePos + 1) == mBytes.size()) && (mCurrentReadBitInBytePos > mEndBitInBytePos)) {
            return true;
        }
        return false;
    }

    template <class T>
    BitsBuffer& operator<<(const T& value)
    {
        const auto valueSize = sizeof(value) * BITS_IN_BYTE;
        for (size_t i = 0; i < valueSize; ++i) {
            *this << static_cast<bool>((value >> i) & 1);
        }
        return *this;
    }

    template <class T>
    const BitsBuffer& operator>>(T& value) const
    {
        const auto valueSize = sizeof(value) * BITS_IN_BYTE;
        for (size_t i = 0; i < valueSize; ++i) {
            bool bit = false;
            *this >> bit;
            if (bit) {
                value |= 1UL << i;
            } else {
                value &= ~(1UL << i);
            }
        }
        return *this;
    }

private:
    std::vector<unsigned char> mBytes;
    unsigned char mEndBitInBytePos;
    mutable size_t mCurrentReadBytePos;
    mutable unsigned char mCurrentReadBitInBytePos;
};

template <>
inline BitsBuffer& BitsBuffer::operator<< <bool>(const bool& value)
{
    if (mEndBitInBytePos == LAST_BIT_IN_BYTE_POS) {
        mBytes.push_back(0x00);
    }
    incrementBitInBytePos(mEndBitInBytePos);
    if (value) {
        mBytes.back() |= 1 << mEndBitInBytePos;
    } else {
        mBytes.back() &= ~(1 << mEndBitInBytePos);
    }
    return *this;
}

template <>
inline BitsBuffer& BitsBuffer::operator<< <std::vector<unsigned char>>(const std::vector<unsigned char>& value)
{
    mBytes.insert(mBytes.end(), value.begin(), value.end());
    return *this;
}

template <>
inline const BitsBuffer& BitsBuffer::operator>><bool>(bool& value) const
{
    value = (mBytes.at(mCurrentReadBytePos) >> mCurrentReadBitInBytePos) & 1;
    incrementBitInBytePos(mCurrentReadBitInBytePos);
    if (mCurrentReadBitInBytePos == 0) {
        mCurrentReadBytePos++;
    }
    return *this;
}
