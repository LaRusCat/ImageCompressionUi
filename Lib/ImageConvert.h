#pragma once

#include "BarchImage.h"
#include "BitmapImage.h"
#include <algorithm>

namespace image {
namespace convertion {

    template <class Source, class Target>
    Target convert(Source source);

    // Emptynes bits
    // 1 - whole white line
    // 0 - not whole white line

    // Colors bits
    // 0 - white chunk
    // 10 black chunk
    // 11 mixed chunk
    template <>
    BitmapImage convert(BarchImage source)
    {
        const auto& sourceHeight = source.height();
        const auto& sourceWidth = source.width();
        const auto& sourceEmptinessBits = source.emptinesBits();
        const auto& sourceDataBits = source.dataBits();
        if (sourceWidth == 0 || sourceHeight == 0) {
            return {};
        }

        static const std::vector<unsigned char> whiteChunk(BarchImage::CHUNK_SIZE, WHITE_COLOR);
        static const std::vector<unsigned char> blackChunk(BarchImage::CHUNK_SIZE, BLACK_COLOR);
        const std::vector<unsigned char> whiteLine(sourceWidth, WHITE_COLOR);

        std::vector<unsigned char> bitmapData;
        bitmapData.reserve(sourceWidth * sourceHeight);

        bool bitBuf = false;
        unsigned char colorBuf = 0x00;

        for (size_t line = 0; line < sourceHeight; ++line) {
            if (sourceEmptinessBits.endOfBits()) {
                throw std::runtime_error("BarchImage::toBitmap Not enough emptiness bits");
                return {};
            }
            sourceEmptinessBits >> bitBuf;
            if (bitBuf) {
                bitmapData.insert(bitmapData.end(), whiteLine.begin(), whiteLine.end());
                continue;
            }
            size_t addedPixels = 0;
            while (addedPixels < sourceWidth) {
                if (sourceDataBits.endOfBits()) {
                    throw std::runtime_error("BarchImage::toBitmap Not enough data bits");
                    return {};
                }
                sourceDataBits >> bitBuf;
                if (!bitBuf) {
                    bitmapData.insert(bitmapData.end(), whiteChunk.begin(), whiteChunk.end());
                    addedPixels += whiteChunk.size();
                } else {
                    if (sourceDataBits.endOfBits()) {
                        throw std::runtime_error("BarchImage::toBitmap No second bit when needed");
                        return {};
                    }
                    sourceDataBits >> bitBuf;
                    if (!bitBuf) {
                        bitmapData.insert(bitmapData.end(), blackChunk.begin(), blackChunk.end());
                        addedPixels += blackChunk.size();
                    } else {
                        for (size_t i = 0; i < BarchImage::CHUNK_SIZE; ++i) {
                            if (sourceDataBits.endOfBits()) {
                                throw std::runtime_error("BarchImage::toBitmap Not enough color bytes");
                                return {};
                            }
                            sourceDataBits >> colorBuf;
                            bitmapData.push_back(colorBuf);
                            ++addedPixels;
                        }
                    }
                }
            }
        }
        if (bitmapData.size() != sourceWidth * sourceHeight) {
            throw std::runtime_error("BarchImage::toBitmap Incorrect converted image size");
            return {};
        }

        return BitmapImage(sourceWidth, sourceHeight, bitmapData);
    }

    template <>
    BarchImage convert(BitmapImage source)
    {
        static const auto allWhitePredicate = [](const auto color) { return color == WHITE_COLOR; };
        static const auto allBlackPredicate = [](const auto color) { return color == BLACK_COLOR; };

        const auto& sourceWidth = source.width();
        const auto& sourceHeight = source.height();
        const auto& sourceData = source.data();

        BitsBuffer emptynessBits;
        BitsBuffer dataBits;
        for (size_t line = 0; line < sourceHeight; ++line) {
            const auto lineBegin = sourceData.begin() + sourceWidth * line;
            const auto lineEnd = lineBegin + sourceWidth;
            const auto isEmptyLine = std::all_of(lineBegin, lineEnd, allWhitePredicate);
            emptynessBits << isEmptyLine;
            if (isEmptyLine)
                continue;

            for (size_t column = 0; column < sourceWidth; column += BarchImage::CHUNK_SIZE) {
                const auto chunkBegin = lineBegin + column;
                const auto chunkEnd = lineBegin + column + BarchImage::CHUNK_SIZE;

                if (all_of(chunkBegin, chunkEnd, allWhitePredicate)) {
                    dataBits << false;
                } else if (all_of(chunkBegin, chunkEnd, allBlackPredicate)) {
                    dataBits << true << false;
                } else {
                    dataBits << true << true;
                    for (auto it = chunkBegin; it < chunkEnd; ++it) {
                        dataBits << *it;
                    }
                }
            }
        }
        return BarchImage(sourceWidth, sourceHeight, emptynessBits, dataBits);
    }

    template <class Source, class Target>
    void convert(const std::filesystem::path& sourceFile,
        const std::filesystem::path& targetFile)
    {
        Source source;
        source.loadFromFile(sourceFile);
        Target target = convert<Source, Target>(source);
        target.saveToFile(targetFile);
    }

} // namespace convertion
} // namespace image
