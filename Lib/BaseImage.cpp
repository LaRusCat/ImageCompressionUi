#include "BaseImage.h"

BaseImage::BaseImage()
    : mWidth(0)
    , mHeight(0)
{
}

BaseImage::BaseImage(const size_t& width, const size_t& height)
    : mWidth(width)
    , mHeight(height)
{
}

const size_t &BaseImage::width() const
{
    return mWidth;
}

const size_t &BaseImage::height() const
{
    return mHeight;
}
