#include "BitmapImage.h"
#include <fstream>
#include <iostream>

#pragma pack(push, 1)

namespace {

const uint16_t BMP_FILE_TYPE = 0x4D42;
struct BMPInfoHeader {
    uint32_t size { sizeof(BMPInfoHeader) }; // Size of this header (in bytes)
    int32_t width { 0 }; // width of bitmap in pixels
    int32_t height { 0 }; // width of bitmap in pixels
                          //       (if positive, bottom-up, with origin in lower left corner)
                          //       (if negative, top-down, with origin in upper left corner)
    uint16_t planes { 1 }; // No. of planes for the target device, this is always 1
    uint16_t bitCount { 8 }; // No. of bits per pixel
    uint32_t compression { 0 }; // 0 or 3 - uncompressed. THIS PROGRAM CONSIDERS ONLY UNCOMPRESSED BMP images
    uint32_t sizeImage { 0 }; // 0 - for uncompressed images
    int32_t xPixelsPerMeter { 0 };
    int32_t yPixelsPerMeter { 0 };
    uint32_t colorsUsed { 0 }; // No. color indexes in the color table. Use 0 for the max number of colors allowed by bit_count
    uint32_t colorsImportant { 0 }; // No. of colors used for displaying the bitmap. If 0 all colors are required
};

struct BMPFileHeader {
    uint16_t fileType { BMP_FILE_TYPE }; // File type always BM which is 0x4D42 (stored as hex uint16_t in little endian)
    uint32_t fileSize { 0 }; // Size of the file (in bytes)
    uint16_t reserved1 { 0 }; // Reserved, always 0
    uint16_t reserved2 { 0 }; // Reserved, always 0
    uint32_t offsetData { sizeof(BMPFileHeader) + sizeof(BMPInfoHeader) }; // Start position of pixel data (bytes from the beginning of the file)
};
}

BitmapImage::BitmapImage()
    : BaseImage()
{
}

BitmapImage::BitmapImage(size_t width, size_t height, const std::vector<unsigned char>& data)
    : BaseImage(width, height)
    , mData(data)
{
}

void BitmapImage::loadFromFile(const std::filesystem::path& filePath)
{
    BMPFileHeader fileHeader;
    BMPInfoHeader bmpInfoHeader;
    std::ifstream fileStream(filePath, std::ios_base::binary);
    if (!fileStream) {
        throw std::runtime_error("Unable to open image file!");
    }
    fileStream.read((char*)&fileHeader, sizeof(fileHeader));
    if (fileHeader.fileType != BMP_FILE_TYPE) {
        throw std::runtime_error("Unrecognized file format!");
    }
    fileStream.read((char*)&bmpInfoHeader, sizeof(bmpInfoHeader));

    if (bmpInfoHeader.bitCount != 8) {
        throw std::runtime_error("Not supported bit count!");
    }
    if (bmpInfoHeader.height < 0) {
        throw std::runtime_error("Origin isn't in the bottom left corner!");
    }
    if (bmpInfoHeader.width % 4 != 0) {
        throw std::runtime_error("Width is not a multiple of 4!");
    }

    fileStream.seekg(fileHeader.offsetData, fileStream.beg);
    mWidth = bmpInfoHeader.width;
    mHeight = bmpInfoHeader.height;
    mData.resize(mWidth * mHeight);
    fileStream.read((char*)mData.data(), mData.size());
}

void BitmapImage::saveToFile(const std::filesystem::path& filePath) const
{
    // TODO It doesn't work well and save corrupted image
    BMPFileHeader fileHeader;
    BMPInfoHeader bmpInfoHeader;
    std::ofstream fileStream(filePath, std::ios_base::binary);
    if (!fileStream) {
        throw std::runtime_error("Unable to open image file!");
    }
    fileHeader.fileSize = fileHeader.offsetData + mData.size();
    bmpInfoHeader.width = mWidth;
    bmpInfoHeader.height = mHeight;

    fileStream.write((const char*)&fileHeader, sizeof(fileHeader));
    fileStream.write((const char*)&bmpInfoHeader, sizeof(bmpInfoHeader));
    for (size_t line = 0; line < mHeight; ++line) {
        fileStream.write((const char*)(mData.data() + mWidth * line), mWidth);
    }
}

std::vector<unsigned char> BitmapImage::data() const
{
    return mData;
}

bool operator==(const BitmapImage& i1, const BitmapImage& i2)
{
    return (i1.width() == i2.width() && i1.height() == i2.height() && i1.data() == i2.data());
}
