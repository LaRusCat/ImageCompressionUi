#pragma once
#include <filesystem>

const unsigned char WHITE_COLOR = 0xFF;
const unsigned char BLACK_COLOR = 0x00;

class BaseImage {
public:
    BaseImage();
    BaseImage(const size_t& width, const size_t& height);
    virtual void loadFromFile(const std::filesystem::path& filePath) = 0;
    virtual void saveToFile(const std::filesystem::path& filePath) const = 0;

    const size_t& width() const;
    const size_t& height() const;

protected:
    size_t mWidth;
    size_t mHeight;
};
