#pragma once

#include "BaseImage.h"
#include <vector>

class BitmapImage : public BaseImage {
public:
    BitmapImage();
    BitmapImage(size_t width, size_t height, const std::vector<unsigned char>& data);
    void loadFromFile(const std::filesystem::path& filePath) override;
    void saveToFile(const std::filesystem::path& filePath) const override;

    std::vector<unsigned char> data() const;

    friend bool operator==(const BitmapImage& c1, const BitmapImage& c2);

private:
    std::vector<unsigned char> mData;
};
