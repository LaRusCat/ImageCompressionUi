#pragma once
#include "BaseImage.h"
#include "BitsBuffer.h"

class BitmapImage;

class BarchImage : public BaseImage {
public:
    BarchImage();
    BarchImage(const size_t& width, const size_t& height, const BitsBuffer& emptinessBits, const BitsBuffer& dataBits);
    void loadFromFile(const std::filesystem::path& filePath) override;
    void saveToFile(const std::filesystem::path& filePath) const override;
    const BitsBuffer& dataBits() const;
    const BitsBuffer& emptinesBits() const;

    static const int CHUNK_SIZE = 4;

private:
    BitsBuffer mEmptinesBits;
    BitsBuffer mDataBits;
};
