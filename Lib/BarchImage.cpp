#include "BarchImage.h"
#include <algorithm>
#include <bitset>
#include <fstream>
#include <iostream>

#pragma pack(push, 1)

namespace {
struct BarchInfoHeader {
    uint64_t width { 0 };
    uint64_t height { 0 };
    uint64_t emptinessBitsSize { 0 };
    uint64_t dataBitsSize { 0 };
};

}

BarchImage::BarchImage()
    : BaseImage()
{
}

BarchImage::BarchImage(const size_t& width, const size_t& height, const BitsBuffer& emptinessBits, const BitsBuffer& dataBits)
    : BaseImage(width, height)
    , mEmptinesBits(emptinessBits)
    , mDataBits(dataBits)
{
}

void BarchImage::loadFromFile(const std::filesystem::path& filePath)
{
    std::ifstream fileStream(filePath, std::ios_base::binary);
    if (!fileStream) {
        throw std::runtime_error("Unable to open image file!");
    }
    BarchInfoHeader infoHeader;
    fileStream.read((char*)&infoHeader, sizeof(infoHeader));
    mWidth = infoHeader.width;
    mHeight = infoHeader.height;

    std::vector<unsigned char> emptinessBits(infoHeader.emptinessBitsSize);
    fileStream.read((char*)emptinessBits.data(), emptinessBits.size());
    mEmptinesBits = BitsBuffer(std::move(emptinessBits));
    std::vector<unsigned char> dataBits(infoHeader.dataBitsSize);
    fileStream.read((char*)dataBits.data(), dataBits.size());
    mDataBits = BitsBuffer(std::move(dataBits));
}

void BarchImage::saveToFile(const std::filesystem::path& filePath) const
{
    std::ofstream fileStream(filePath, std::ios_base::binary);
    if (!fileStream) {
        throw std::runtime_error("Unable to open image file!");
    }
    BarchInfoHeader infoHeader = { .width = mWidth, .height = mHeight, .emptinessBitsSize = mEmptinesBits.bytes().size(), .dataBitsSize = mDataBits.bytes().size() };
    fileStream.write((const char*)&infoHeader, sizeof(infoHeader));
    fileStream.write((const char*)mEmptinesBits.bytes().data(), mEmptinesBits.bytes().size());
    fileStream.write((const char*)mDataBits.bytes().data(), mDataBits.bytes().size());
}

const BitsBuffer&
BarchImage::dataBits() const
{
    return mDataBits;
}

const BitsBuffer& BarchImage::emptinesBits() const
{
    return mEmptinesBits;
}
